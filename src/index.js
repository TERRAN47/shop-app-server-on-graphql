import config from 'config';
import http from 'http';
import { graphql } from './Services';
import app from './app';
import { DEFAULT_PORT } from 'config';

const initServer = async () => {

  const koaServer = http.createServer(app.callback())
  let apolloServer = await graphql.apollo(app, koaServer);

  koaServer.listen(DEFAULT_PORT, () => {
    console.log(`🚀 Server ready at http://185.251.89.177:${DEFAULT_PORT}${apolloServer.graphqlPath}`)
  });

}

initServer();