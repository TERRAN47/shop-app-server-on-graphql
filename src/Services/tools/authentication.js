import jwt from 'jsonwebtoken';
import {
	users
} from '../../Models';

import {
  ApolloError
} from "apollo-server-errors";

import { SECRET_KEY } from 'config';

const authentication = async (token) => {

	let checkToken = await jwt.verify(token, SECRET_KEY, async (err, decoded) => {

  	if(err){
	  	return false;
  	} else {

  		let findUser = await users.findOne({
  		  email:decoded.email
  		}).deepPopulate('role avatar');

  		if(findUser){

			  return findUser;

  		}

  		return false;

  	}
	});

	return checkToken;

}

export const checkAuthentication = async (ctx) => {

  if(!ctx.isAuth && !ctx.user){
    throw new ApolloError('Not authorized', 500, null);
    return
  }

}

export default authentication;