import mongoose from 'mongoose';
import {
	DATABASE_PORT,
	DATABASE_HOST,
	DATABASE_NAME,
	DATABASE_USER,
	DATABASE_PWD
} from 'config';
 
try {
  mongoose.connect(`mongodb://${DATABASE_HOST}:${DATABASE_PORT}/${DATABASE_NAME}`, {
    auth: {
      user: DATABASE_USER,
      password: DATABASE_PWD
    },
    autoIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
}catch(err){
  console.log(33, err)
}

export default mongoose;