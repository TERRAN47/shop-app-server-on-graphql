
const checkRatingForUser = async (params, modelReviews, error) => {

  let {
    product_id
  } = params.params.fields;

  let { user } = params;

  let checkRating = await modelReviews.findOne({
    user:user._id,
    product:product_id
  });

  if(checkRating){
    throw new error('Вы уже ставили оценку для этого продукта', 200, null);
    return
  }

}

export default {
  checkRatingForUser
}