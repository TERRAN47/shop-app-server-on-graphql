import { 
  ApolloError
} from "apollo-server-errors";

const statusOrder = async (status) => {

  if(status != 'CREATED' || status != 'IN_PROCCESS' || status != 'DELIVERY' || status != 'COMPLETED'){
    throw new ApolloError('Not a valid field "status"', 500, null);
  }

  return true;

}

export default statusOrder;