import {
  ApolloError
} from "apollo-server-errors";

const isAdmin = async (ctx) => {

  if(ctx.user){
    if(!ctx.isAuth || ctx.user.role.role != 'ADMIN'){
      throw new ApolloError('You are not an admin', 'UNAUTHENTICATED', {

      });
      return
    }
    return
  }

  throw new ApolloError('Not authorized', 500, null);
  return

}

export default isAdmin;