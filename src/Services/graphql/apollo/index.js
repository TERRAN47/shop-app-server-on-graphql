import { ApolloServer } from 'apollo-server-koa';
import { SubscriptionServer } from "subscriptions-transport-ws";
import authentication from '../../tools/authentication';
import { execute, subscribe } from "graphql";
import { ApolloServerPluginLandingPageGraphQLPlayground  } from 'apollo-server-core';

import typeDefs from '../types';
import resolvers from '../resolvers';

import { makeExecutableSchema } from "@graphql-tools/schema";

const apollo = async (app, koaServer) => {

  const schema = makeExecutableSchema({
    typeDefs,
    resolvers,
  });

  const subscriptionServer = SubscriptionServer.create({
    schema,
    execute,
    subscribe,
    onConnect() {
      console.log(333, 'WS')
      return
    },
  }, {
    server: koaServer,
    path: '/graphql',
  });

  const createApolloServer = new ApolloServer({
    schema,
    playground: true,
    introspection: true,
    context: async (req) => {
      let token = null;
      if(req.ctx && req.ctx.req && req.ctx.req.headers){
        token = req.ctx.req.headers.authorization
      }
      let isAuth = await authentication(token);
      return { 
        isAuth: isAuth ? true : false,
        user: isAuth ? isAuth : null,
        token
      };

    },
    plugins: [ApolloServerPluginLandingPageGraphQLPlayground(),{
      async serverWillStart() {
        return {
          async drainServer() {
            subscriptionServer.close();
          }
        };
      }
    }],
    formatError: (err) => {
      return new Error(err.message);
    }
  });

  await createApolloServer.start();
  createApolloServer.applyMiddleware({app});
  return createApolloServer;

}

export default apollo;