const arithmeticMean = async (reviews, newRating) => {

  let mainRatingRes = 0;
  let ratingArr = [];

  reviews.forEach((ratVal)=>{
    console.log(Number(ratVal.rating))
    ratingArr = [...ratingArr, Number(ratVal.rating)];
  })

  ratingArr = [...ratingArr, Number(newRating)];

  ratingArr.forEach((ratVal)=>{
    mainRatingRes = mainRatingRes + ratVal;
  })

  return mainRatingRes/ratingArr.length;

}

export default {
  arithmeticMean
}