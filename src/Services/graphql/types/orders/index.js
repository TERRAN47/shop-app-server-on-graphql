
const orderSchema = `

  type Query {
    getOrder(id:String!): Order
  	getOrders(query:QueryInput): [Order]
  }

  type Mutation {
    createOrder(order:OrderInput): Order
    changeStatusOrder(order:OrderStatusInput): Order
  }

  input OrderStatusInput {
    id: String
    status: String
  }

  input OrderInput {
    price: Int
    products: [String]
  }

  type Order {
    _id: String
    order_number: String
    user: User
    price: Int
    products: [Product]
    createdAt: String
    status: String
  }

`;

export default orderSchema;