
const settingSchema = `
type Query {
	getSettings(query: SettingQuery) : [Setting]
}

type Mutation {
	saveSettings(setting: SettingInput!) : Setting!
}

input SettingInput {
  type: String!
  value: String!
}

type Setting {
  type: String
  value: String
}

type TypeValue {
  value:String
}

input SettingQuery {
  type: String
}
`;

export default settingSchema;