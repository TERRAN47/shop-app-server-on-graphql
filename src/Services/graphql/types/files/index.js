
const fileSchema = `
  scalar Upload

  type Mutation {
    uploadFile(file: Upload!) : File
  }

  type File {
    _id: String
    type: String
    url: String
  }

  type Picture {
    _id: String
    type: String
    url: String
  }

`;

export default fileSchema;