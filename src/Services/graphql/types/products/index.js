
const productSchema = `

  type Query {
  	getProducts(query:QueryInput): [Product]
  }

  type Mutation {
    createProduct(product: productInput!) : Product
    updateProduct(product: productUpdateInput!) : Product
    removeProduct(id: String!) : RemoveStatus
  }

  type Subscription {
    getproductsubs: Product
  }

  input productInput {
    title: String!
    picture: String!
    description: String
    price: Int!
    category: String!
  }

  input productUpdateInput {
    id: String!
    title: String!
    picture: String!
    description: String
    price: Int!
    category: String!
  }

  type Product {
    _id: String
    title: String
    picture: Picture
    description: String
    price: Int
    category: Category
    rating: Rating
  }

`;

export default productSchema;