
import { mergeTypeDefs } from '@graphql-tools/merge';

//schemas
import userSchema from './user';
import categorySchema from './categories';
import fileSchema from './files';
import products from './products';
import settingSchema from './settings';
import orderSchema from './orders';
import ratingSchema from './rating';

const typeDefs = [
  userSchema,
  categorySchema,
  fileSchema,
  products,
  settingSchema,
  orderSchema,
  ratingSchema
];

export default mergeTypeDefs(typeDefs);