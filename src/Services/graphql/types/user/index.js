
const userSchema = `
type Query {
	getUser: User
  getUsers(query: QueryInput): [User]
}

type Mutation {
	authentication(profile: UserInput) : User
	changeAvatar(file: Upload!): File!
  updateUser(profile: UserInputUpdate!): User!
}

input QueryInput {
  query: String
  page: Int
  limit: Int
}

input UserInput {
	firstName: String
	email: String!
	avatar: String
  phone: String
	password: String!
  type: String!
}

input UserInputUpdate {
  firstName: String
  avatar: String
  phone: String
}

type Role {
  role: String
}

type User {
	_id: String
	firstName: String
	email: String
	avatar: String
	token: String
	message: String
  role: Role
  createdAt: String
  phone: String
}
`;

export default userSchema;