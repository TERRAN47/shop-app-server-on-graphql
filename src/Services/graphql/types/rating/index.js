
const ratingSchema = `

  type Mutation {
    ratingProduct(fields:ratingProductInput!) : Boolean
  }

  input ratingProductInput {
    new_rating_product: Int!
    product_id: String!
    review: String
  }

  type Rating {
    rating: String
    count: Int
  }

`;

export default ratingSchema;