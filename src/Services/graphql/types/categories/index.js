
const categorySchema = `
type Query {
	getCategories(type: String): [Category]
  getSubCategories(id: String): [Category]
}

type Mutation {
	createCategory(category: categoryInput) : Category
  removeCategory(cat_id: String) : RemoveStatus
}

type RemoveStatus {
  status: Boolean
  id: String
}

type IDCategory {
  id: String
}

input categoryInput {
	title: String!
	type: String!
  picture: String
  parent_category: String
}

type Category {
  _id: String
  title: String
  type: String
  picture: Picture
  parent_category: Category
}

`;

export default categorySchema;