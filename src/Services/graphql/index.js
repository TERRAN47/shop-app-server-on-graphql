
import apollo from './apollo';
import typeDefs from './types';
import resolvers from './resolvers';

const graphql = {
	apollo,
	typeDefs,
	resolvers
}

export default graphql;
