import {
	ApolloError
} from "apollo-server-errors";
import path from 'path';
import fs from 'fs';
import generator from 'generate-password';

import {
  files
} from '../../../../Models';

import filesize from 'filesize';

import {
  checkAuthentication,
  isAdmin
} from '../../middlewares';

const filesResolvers = {
  Mutation: {
    uploadFile: async (_, params, ctx) => {

      await checkAuthentication(ctx);

      let { file } = params;

      try {

        let { createReadStream, filename, mimetype, encoding } = file.file;
        filename = filename.replace(/\s+/g, '').replace('(', '').replace(')', '');

        const generatePrefix = generator.generate({
          length: 10,
          numbers: true
        });

        let defaultURL = `uploads/${generatePrefix}_${filename}`;

        const stream = createReadStream();
        const pathName = path.join(__dirname, `../../../../../public/${defaultURL}`);
        await stream.pipe(fs.createWriteStream(pathName))

        let saveFile = await files.create({
          url:defaultURL,
          type:'IMAGE'
        });

        return saveFile;

      }catch(err){
        throw new ApolloError('Error upload file', 500, null);
        return
      }

    }
  }
}

export default filesResolvers;