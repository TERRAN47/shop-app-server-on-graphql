import {
	ApolloError
} from "apollo-server-errors";

import {
  orders
} from '../../../../Models';
import PhoneNumber from 'awesome-phonenumber';
import generator from 'generate-password';

import {
  statusOrder,
  isAdmin
} from '../../middlewares';

const Orders = {
  Query: {
    getOrder: async (_, params, args) => {

      try {

        let getOrder = await orders.findOne({
          _id:params.id
        }).deepPopulate('user products user.role');

        return getOrder;

      }catch(err){
        throw new ApolloError('Error', 500, null);
      }

    },
    getOrders: async (_, params, args) => {

      let query = {};
      let {
        page = 1,
        limit = 10
      } = params.query;

      if(params.query.query){

        let pn = new PhoneNumber(params.query.query);

        if(pn.isValid()){
          query = {
            'userField.phone':params.query.query.replace('+', '')
          }
        }else{
          query = {
            $or:[{
              'userField.firstName': { "$regex": params.query.query, "$options": "i" }
            }, {
              order_number: params.query.query
            }, {
              'userField.email': params.query.query
            }]
          }
        }

      }

      try {

        let getOrders = await orders.aggregate([
          {
            $lookup:  {
              from: 'users',
              localField: 'user',
              foreignField: '_id',
              as: 'userField'
            }
          },
          {
            $match:query
          },
          {$skip: (page-1 ) * limit},
          {$limit:limit},
        ]);

        await orders.deepPopulate(getOrders, "user products products.picture products.category.parent_category products.category products.category.picture");

        return getOrders;

      }catch(err){
        console.log(4444, err)
        throw new ApolloError('Error', 500, null);
      }

    }
  },
  Mutation: {
    changeStatusOrder: async (_, params, ctx) => {

      let {
        id,
        status
      } = params.query;

      await isAdmin(ctx);
      await statusOrder(status);

      try {

        await orders.updateOne({
          _id:id
        }, {
          status
        });

        let getOrder = await orders.findOne({
          _id:id
        });

        return getOrder;

      }catch(err){
        throw new ApolloError('Error', 500, null);
      }

    },
    createOrder: async (_, params, ctx) => {

      await isAdmin(ctx);

      try {

        let genOrder = generator.generate({
          length: 5,
          numbers: true
        });

        let createOrder = await orders.create({
          user:ctx.user._id,
          price:params.order.price,
          order_number:genOrder,
          products:params.order.products,
          status:'CREATED'
        });

        let getOrder = await orders.findOne({
          _id:createOrder._id
        }).deepPopulate('user products user.role user.avatar');

        return getOrder;

      }catch(err){
        throw new ApolloError('Error', 500, null);
      }

    }
  }
}

export default Orders;