import {
	ApolloError
} from "apollo-server-errors";

import {
  settings
} from '../../../../Models';

import {
  checkAuthentication,
  isAdmin
} from '../../middlewares';

const user = {
  Query: {
    getSettings: async (_, params, ctx) => {

      await checkAuthentication(ctx);

      let query = {};

      if(params.query){
        query = params.query;
      }

      try {

        let getSettings = await settings.find(query);
        return getSettings;

      }catch(err){
        throw new ApolloError('Error', 500, null);
        return
      }

    }
  },
  Mutation: {
    saveSettings: async (_, params, ctx) => {
      await isAdmin(ctx);

      try {

        let getSetting = await settings.findOne({
          type:params.setting.type
        });

        if(getSetting){
          let updateSetting = await settings.updateOne({
            type:params.setting.type
          }, {
            ...params.setting
          });

          return params.setting;

        }else{

          let createSetting = await settings.create({
            ...params.setting
          });

          return createSetting._doc;

        }

      }catch(err){
        throw new ApolloError('Error', 500, null);
        return
      }

    }
  }
}

export default user;