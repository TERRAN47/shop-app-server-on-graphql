import {
	ApolloError
} from "apollo-server-errors";

import {
  users,
  roles
} from '../../../../Models';
import jwt from 'jsonwebtoken';
import { SECRET_KEY } from 'config';
import passwordHash from 'password-hash';
import PhoneNumber from 'awesome-phonenumber';

import {
  checkAuthentication,
  isAdmin
} from '../../middlewares';

const user = {
  Query: {
    getUser: async (_, __, ctx) => {

      await checkAuthentication(ctx);

      try {
        return ctx.user;
      }catch(err){
        throw new ApolloError('Error', 500, null);
      }

    },
    getUsers: async (_, params, ctx) => {

      await isAdmin(ctx);

      let query = {};
      let {
        page = 1,
        limit = 10
      } = params.query;

      if(params.query.query){

        let pn = new PhoneNumber(params.query.query);

        if(pn.isValid()){
          query = {
            phone:params.query.query.replace('+', '')
          }
        }else{
          query = {
            firstName: { "$regex": params.query.query, "$options": "i" }
          }
        }

      }

      try {

        let getUsers = await users.find(query, null, {
          limit: limit,
          skip: ( page-1 ) * limit,
        }).deepPopulate('avatar');

        return getUsers;

      }catch(err){
        throw new ApolloError('Error', 500, null);
      }

    },
  },
  Mutation: {
    changeAvatar: async (_, params, ctx) => {

      await checkAuthentication(ctx);

      return

    },
    updateUser:async (_, params, ctx) => {

      await checkAuthentication(ctx);

      let {
        firstName,
        avatar,
        phone
      } = params.profile;

      try {

        let updateUser = await users.updateOne({
          _id:String(ctx.user._id)
        }, params.profile);

        let getUser = await users.findOne({
          _id:String(ctx.user._id)
        });

        return getUser._doc;

      }catch(err){
        throw new ApolloError('Error', 500, null);
        return
      }

    },
    authentication: async (_, params, ctx) => {

      const {
        firstName = '',
        email,
        avatar,
        password,
        type
      } = params.profile;

      let objectUser;

      const findUser = await users.findOne({
        email
      }).deepPopulate('role avatar');

      if(findUser){

        if(!passwordHash.verify(password, findUser._doc.password)){
          throw new ApolloError('Неверный пароль', 500, null);
          return
        }

        if(type == 'ADMIN' && findUser._doc.role.role != 'ADMIN'){
          throw new ApolloError('Нет прав', 401, null);
          return
        }

        objectUser = findUser._doc;

        let userGenerateToken = jwt.sign(objectUser, SECRET_KEY, {
          algorithm:'HS512',
          expiresIn: '24h'
        });

        return {
          ...objectUser,
          token:userGenerateToken
        }

      }

      let createUser = await users.create({
        firstName,
        email,
        avatar,
        password:passwordHash.generate(password)
      });

      let createRole = await roles.create({
        user_id:createUser._id,
        role:'USER'
      });

      await users.update({
        _id:createUser._id
      }, {
        role:createRole._id
      });

      let userGenerateToken = jwt.sign(createUser._doc, SECRET_KEY, {
        algorithm:'HS512',
        expiresIn: '24h'
      });

      if(type == 'ADMIN'){
        throw new ApolloError('Нет прав', 401, null);
        return
      }

      return {
        ...createUser._doc,
        token:userGenerateToken
      }

    }
  }
}

export default user;