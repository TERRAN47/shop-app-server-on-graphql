import {
	ApolloError
} from "apollo-server-errors";
import pubsub from '../../subscription';

import {
  products
} from '../../../../Models';

import {
  isAdmin
} from '../../middlewares';

const categoriesResolvers = {
  Subscription: {
    getproductsubs: {
      subscribe: (ctx) => {
        return pubsub.asyncIterator(['GET_PRODUCTS_SUBS'])
      }
    },
  },
  Query: {
    getProducts: async (_, params, ctx) => {

      let query = {};
      let {
        page = 1,
        limit = 10
      } = params.query;

      if(params.query.query){
        query = {
          title: { "$regex": params.query.query, "$options": "i" }
        }
      }

    	try {

        let getProducts = await products.find(query, null, {
          sort: {
            createdAt: -1
          },
          limit: limit,
          skip: ( page-1 ) * limit
        }).deepPopulate('picture category category.picture category.parent_category rating');

        pubsub.publish('GET_PRODUCTS_SUBS', {
          getproductsubs: getProducts[0]
        });
        return getProducts;

      }catch(err){
        throw new ApolloError('Error', 500, null);
        return
      }

    },
  },
  Mutation: {
    updateProduct: async (_, params, ctx) => {

      await isAdmin(ctx);

      let {
        id,
        title,
        picture,
        description,
        price,
        category,
      } = params.product;

      try {

        await products.updateOne({
          _id:String(id)
        }, {
          title,
          picture,
          description,
          price,
          category,
        });

        let getProduct = await products.findOne({
          _id:String(id)
        });

        return getProduct;

      }catch(err){
        console.log(222, err)
        throw new ApolloError('Error', 500, null);
        return
      } 

    },
    removeProduct: async (_, params, ctx) => {

      await isAdmin(ctx);

      let {
        id
      } = params;

      try {

        await products.findOne({
          _id:id
        }).remove();

        return {
          status:true
        };

      }catch(err){
        throw new ApolloError('Error', 500, null);
        return
      } 

    },
    createProduct: async (_, params, ctx) => {

      await isAdmin(ctx);

      let {
        title,
        picture,
        description,
        price,
        category
      } = params.product;

      try {

        let createProduct = await products.create({
          title,
          picture,
          description,
          price,
          category
        });

        let findCreatedProduct = await products.findOne({
          _id:createProduct._id
        }).deepPopulate('picture category');

        return findCreatedProduct;

      }catch(err){
        throw new ApolloError('Error', 500, null);
        return
      }

    }
  }
}

export default categoriesResolvers;