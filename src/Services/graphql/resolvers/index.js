import _ from 'lodash';

//schemas
import user from './user';
import categories from './categories';
import files from './files';
import products from './products';
import settings from './settings';
import orders from './orders';
import rating from './rating';

const resolvers = {
	Query: {
		...user.Query,
    ...categories.Query,
    ...products.Query,
    ...settings.Query,
    ...orders.Query,
	},
  Mutation: {
    ...user.Mutation,
    ...categories.Mutation,
    ...files.Mutation,
    ...products.Mutation,
    ...settings.Mutation,
    ...orders.Mutation,
    ...rating.Mutation
  },
  Subscription:{
    ...products.Subscription
  }
};

export default resolvers;