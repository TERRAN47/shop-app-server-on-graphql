import {
	ApolloError
} from "apollo-server-errors";
import pubsub from '../../subscription';

import {
  products,
  rating,
  reviews
} from '../../../../Models';

import {
  isAdmin,
  checkAuthentication,
  ratingMiddleware
} from '../../middlewares';

import {
  ratingUtils
} from '../../utils';

const ratingResolver = {
  Mutation: {
    ratingProduct: async (_, params, ctx) => {

      await checkAuthentication(ctx);
      await ratingMiddleware.checkRatingForUser({
        params,
        user:ctx.user
      }, reviews, ApolloError);

      let { user } = ctx;

      let {
        new_rating_product,
        product_id,
        review
      } = params.fields;

      new_rating_product = Number(new_rating_product);
      let ratingRes = 0;

      try {

        let getReviews = await reviews.find({
          product:product_id
        });

        if(getReviews && getReviews.length){
          ratingRes = await ratingUtils.arithmeticMean(getReviews, new_rating_product);

          await rating.update({
            rating:String(ratingRes),
            $inc: { 
              count: +1
            } 
          });

        }else{

          let createRating = await rating.create({
            rating:String(new_rating_product),
            count: 1
          });

          await products.update({
            _id:product_id
          }, {
            rating:createRating._doc._id
          });

        }

        await reviews.create({
          product:product_id,
          user:user._id,
          rating:Number(new_rating_product),
          review
        });

        return true;

      }catch(err){
        console.log(222, err)
        throw new ApolloError('Error', 500, null);
        return
      } 

    }
  }
}

export default ratingResolver;