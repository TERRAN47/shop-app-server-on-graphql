import { 
	ApolloError
} from "apollo-server-errors";

import {
  categories
} from '../../../../Models';

import {
  isAdmin
} from '../../middlewares';

const categoriesResolvers = {
  Query: {
    getCategories: async (_, params, ctx) => {

    	try {

        let getCategories = await categories.find({
          type:params.type ? params.type : 'MAIN'
        }, null, {sort: {
          createdAt: 1
        }}).deepPopulate('picture parent_category');

        return getCategories;

      }catch(err){
        throw new ApolloError('Error', 500, null);
        return
      }

    },
    getSubCategories: async (_, params, ctx) => {

      try {

        let getSubCategories = await categories.find({
          parent_category:params.id
        }, null, {sort: {
          createdAt: -1
        }}).deepPopulate('picture parent_category');

        return getSubCategories;

      }catch(err){
        throw new ApolloError('Error', 500, null);
        return
      }

    },
  },
  Mutation: {
    createCategory: async (_, params, ctx) => {

      await isAdmin(ctx);

      try {

        let createCategory;

        if(params.category.type == 'SUB'){
          createCategory = await categories.create({
            title: params.category.title,
            type: params.category.type,
            parent_category: params.category.parent_category,
            picture: params.category.picture
          });
        }else{
          createCategory = await categories.create({
            title: params.category.title,
            type: params.category.type,
            picture: params.category.picture
          });
        }

        let findCategory = await categories.findOne({
          _id:createCategory._id
        }).deepPopulate('picture parent_category');

        return findCategory;

      }catch(err){
        throw new ApolloError('Error', 500, null);
        return
      }

    },
    removeCategory: async (_, params, ctx) => {

      await isAdmin(ctx);

      try {

        await categories.find({
          $or:[{
            _id:params.cat_id
          },{
            parent_category:params.cat_id
          }]
        }).remove();

        return {
          status: true,
          id: params.cat_id
        };

      }catch(err){
        throw new ApolloError('Error', 500, null);
        return
      }

    }
  }
}

export default categoriesResolvers;