import Koa from 'koa';
import koaBody from 'koa-body';
import cors from 'koa2-cors';
import path from 'path';
import serve from 'koa-static';
import { graphqlUploadKoa } from 'graphql-upload';

const app = new Koa();

const rootPath = path.join(__dirname, '../public');
app.use(serve(rootPath));

app.use(koaBody());
app.use(cors());

app.use(graphqlUploadKoa({ maxFileSize: 10000000, maxFiles: 10 }));

export default app;