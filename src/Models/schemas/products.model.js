import mongoose from '../../Services/connectors/mongo';
import deepPopulate from 'mongoose-deep-populate';

const productSchema = new mongoose.Schema({
  title: {
    type: String,
    default:''
  },
  picture: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'files'  
  },
  description:{
    type: String,
    default:''
  },
  price: {
    type: Number,
    default:0
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'categories'  
  },
  rating: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'rating'  
  }
},{
  timestamps: true
});

productSchema.plugin(deepPopulate(mongoose));

const products = mongoose.model('products', productSchema);

export default products;