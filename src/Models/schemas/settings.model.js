import mongoose from '../../Services/connectors/mongo';
import deepPopulate from 'mongoose-deep-populate';

const settingSchema = new mongoose.Schema({
  type: {
    type: String,
    default:''
  },
  value: {
    type: String,
    default:''
  }
},{
  timestamps: true
});

settingSchema.plugin(deepPopulate(mongoose));

const settings = mongoose.model('settings', settingSchema);

export default settings;