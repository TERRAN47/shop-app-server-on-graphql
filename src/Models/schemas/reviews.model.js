import mongoose from '../../Services/connectors/mongo';
import deepPopulate from 'mongoose-deep-populate';

const reviewSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users'  
  },
  rating:{
    type: Number,
    default:0
  },
  review:{
    type: Number,
    default:0
  },
  product:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'products'  
  }
},{
  timestamps: true
});

reviewSchema.plugin(deepPopulate(mongoose));

const reviews = mongoose.model('reviews', reviewSchema);

export default reviews;