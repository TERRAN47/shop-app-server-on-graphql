import mongoose from '../../Services/connectors/mongo';
import deepPopulate from 'mongoose-deep-populate';

const orderSchema = new mongoose.Schema({
  order_number: {
    type: String,
    default:''
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users'  
  },
  price:{
    type: Number,
    default:0
  },
  products:[{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'products' 
  }],
  status:{
    type: String,
    default:'CREATED' //IN_PROCCESS DELIVERY COMPLETED
  }
},{
  timestamps: true
});

orderSchema.plugin(deepPopulate(mongoose));

const orders = mongoose.model('orders', orderSchema);

export default orders;