import mongoose from '../../Services/connectors/mongo';
import deepPopulate from 'mongoose-deep-populate';

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		default:'anonim'
	},
	email: {
  	type: String,
  	default:''
	},
	avatar:{
  	type: mongoose.Schema.Types.ObjectId,
  	ref: 'files'  
	},
	password: {
  	type: String,
  	default:''
	},
  role: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'roles'  
  },
  phone: {
    type: String,
    default:'' 
  }
},{
  timestamps: true
});

userSchema.plugin(deepPopulate(mongoose));

const users = mongoose.model('users', userSchema);

export default users;