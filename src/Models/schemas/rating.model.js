import mongoose from '../../Services/connectors/mongo';
import deepPopulate from 'mongoose-deep-populate';

const ratingSchema = new mongoose.Schema({
  rating: {
    type: String,
    default:''
  },
  count:{
    type: Number,
    default:0
  }
},{
  timestamps: true
});

ratingSchema.plugin(deepPopulate(mongoose));

const rating = mongoose.model('rating', ratingSchema);

export default rating;