import mongoose from '../../Services/connectors/mongo';
import deepPopulate from 'mongoose-deep-populate';

const fileSchema = new mongoose.Schema({
	type: {
		type: String,
		default:'IMAGE' // AVATAR IMAGE
	},
  url: {
    type: String,
    default:''
  }
},{
  timestamps: true
});

fileSchema.plugin(deepPopulate(mongoose));

const files = mongoose.model('files', fileSchema);

export default files;