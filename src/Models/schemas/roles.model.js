import mongoose from '../../Services/connectors/mongo';
import deepPopulate from 'mongoose-deep-populate';

const roleSchema = new mongoose.Schema({
  user_id: {
    type: String,
    default:''
  },
  role: {
    type: String,
    default:'USER' //ADMIN USER
  }
},{
  timestamps: true
});

const roles = mongoose.model('roles', roleSchema);

export default roles;