import mongoose from '../../Services/connectors/mongo';
import deepPopulate from 'mongoose-deep-populate';

const categorySchema = new mongoose.Schema({
  title: {
    type: String,
    default:''
  },
  type: {
    type: String,
    default:'MAIN' // MAIN SUB
  },
  picture:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'files'  
  },
  parent_category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'categories' 
  }
},{
  timestamps: true
});

categorySchema.plugin(deepPopulate(mongoose));

const categories = mongoose.model('categories', categorySchema);

export default categories;