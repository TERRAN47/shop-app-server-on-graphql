import users from './schemas/users.model';
import files from './schemas/files.model';
import categories from './schemas/categories.model';
import products from './schemas/products.model';
import roles from './schemas/roles.model';
import settings from './schemas/settings.model';
import orders from './schemas/orders.model';
import rating from './schemas/rating.model';
import reviews from './schemas/reviews.model';

export {
	users,
	files,
  categories,
  products,
  roles,
  settings,
  orders,
  rating,
  reviews
}